:gitlab_url: https://gitlab.com/open-kappa/open-kappa.gitlab.io

OPEN-Kappa
==========

**Welcome to OPEN-Kappa documentation!**

There are the following ongoing projects:

* **C, C++, CMake**, SystemC:
    * *MyCMake*:
        * Repo: https://gitlab.com/open-kappa/cpp/mycmake
        * Documentation: https://open-kappa.gitlab.io/cpp/mycmake
    * *SCNSL*:
        * Repo: https://gitlab.com/open-kappa/scnsl
        * Documentation: https://open-kappa.gitlab.io/scnsl
* **Node.js modules**:
    * *mybackup*:
        * Repo: https://gitlab.com/open-kappa/nodejs/mybackup
        * Documentation: https://open-kappa.gitlab.io/nodejs/mybackup
    * *myexe*:
        * Repo: https://gitlab.com/open-kappa/nodejs/myexe
        * Documentation: https://open-kappa.gitlab.io/nodejs/myexe
    * *myjson*:
        * Repo: https://gitlab.com/open-kappa/nodejs/myjson
        * Documentation: https://open-kappa.gitlab.io/nodejs/myjson
    * *mylog*:
        * Repo: https://gitlab.com/open-kappa/nodejs/mylog
        * Documentation: https://open-kappa.gitlab.io/nodejs/mylog
    * *mypromise*:
        * Repo: https://gitlab.com/open-kappa/nodejs/mypromise
        * Documentation: https://open-kappa.gitlab.io/nodejs/mypromise
    * *mytest*:
        * Repo: https://gitlab.com/open-kappa/nodejs/mytest
        * Documentation: https://open-kappa.gitlab.io/nodejs/mytest
* **Node-RED modules**:
    * *node-red-contrib-mydb*:
        * Repo: https://gitlab.com/open-kappa/node-red/node-red-contrib-mydb
        * Documentation: https://open-kappa.gitlab.io/node-red/node-red-contrib-mydb
    * *node-red-contrib-myutils*:
        * Repo: https://gitlab.com/open-kappa/node-red/node-red-contrib-myutils
        * Documentation: https://open-kappa.gitlab.io/node-red/node-red-contrib-myutils
